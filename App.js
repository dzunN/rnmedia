import React, {useState} from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';
import Pdf from 'react-native-pdf';
import Video from 'react-native-video';
import {RPdf, RVideo, Thumbnail} from './assets';

const RNVideo = () => {
  return (
    <View style={styles.page}>
      <Text>My Video Project</Text>
      <Video
        source={RVideo}
        style={styles.backgroundVideo}
        controls={true}
        poster={Thumbnail}
      />
    </View>
  );
};
const RNPdf = () => {
  const [linkPdf, setLinkPdf] = useState('');

  const OpenPdf = () => {
    setLinkPdf(RPdf);
  };

  return (
    <View style={styles.pdfContainer}>
      <Text style={styles.titlePDF}>Document View</Text>
      <Button title="Open Pdf Document" onPress={OpenPdf} />

      {linkPdf ? <Pdf source={linkPdf} style={styles.pdfView} /> : null}
    </View>
  );
};

const App = () => {
  const [page, setPage] = useState('video');
  return (
    <>
      <View style={styles.navigator}>
        <Button title="Video Mode" onPress={() => setPage('video')} />
        <Button title="PDF Mode" onPress={() => setPage('pdf')} />
      </View>
      {page === 'video' ? <RNVideo /> : <RNPdf />}
    </>
  );
};

export default App;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  navigator: {
    backgroundColor: 'blue',
    justifyContent: 'space-around',
    flexDirection: 'row',
  },
  backgroundVideo: {
    width: 200,
    height: 200,
  },
  pdfContainer: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#696969',
  },
  titlePDF: {color: '#ffffff', marginBottom: 10},
  pdfView: {
    marginTop: 10,
    width: 600,
    height: 600,
  },
});
