const LVideo = require('./big_buck_bunny.mp4');
const RVideo = {uri: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4'};

const Thumbnail =
  'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/English_Cocker_Spaniel_4.jpg/800px-English_Cocker_Spaniel_4.jpg';

const LPdf = require('./thereactnativebook-sample.pdf');
const RPdf = {uri: 'http://samples.leanpub.com/thereactnativebook-sample.pdf'};

export {LVideo, RVideo, Thumbnail, RPdf, LPdf};
